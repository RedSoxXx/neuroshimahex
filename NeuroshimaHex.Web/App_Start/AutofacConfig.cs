﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Autofac;
using Autofac.Integration.Mvc;
using Owin;
using System.Configuration;
using System.Web.Http;
using Autofac.Integration.WebApi;
using NeuroshimaHex.Persistence.Repositories;
using NeuroshimaHex.Bll.Contracts;
using NeuroshimaHex.Bll.Contracts.Repositories;
using NeuroshimaHex.Bll.Contracts.Services;
using NeuroshimaHex.Bll.Services.Identity;
using NeuroshimaHex.Bll.Services.Services;
using NeuroshimaHex.Persistence;
using NeuroshimaHex.Web.Controllers;


[assembly: WebActivatorEx.ApplicationShutdownMethod(typeof(NeuroshimaHex.Web.App_Start.AutofacConfig), "Dispose")]

namespace NeuroshimaHex.Web.App_Start
{
    public static class AutofacConfig
    {
        private static IContainer _container;

        public static void RegisterComponents(IAppBuilder app)
        {
            var builder = new ContainerBuilder();

            // Register your MVC controllers.
            builder.RegisterControllers(typeof(MvcApplication).Assembly);

            //Register WebApi Controllers
            builder.RegisterApiControllers(typeof(MvcApplication).Assembly);

            // OPTIONAL: Register model binders that require DI.
            //builder.RegisterModelBinders(Assembly.GetExecutingAssembly());
            //builder.RegisterModelBinderProvider();

            // OPTIONAL: Register web abstractions like HttpContextBase.
            //builder.RegisterModule<AutofacWebTypesModule>();

            // OPTIONAL: Enable property injection in view pages.
            builder.RegisterSource(new ViewRegistrationSource());

            // OPTIONAL: Enable property injection into action filters.
            builder.RegisterFilterProvider();

            // Set the dependency resolver to be Autofac
            builder.RegisterType<NeuroshimaHexContext>().As<DbContext>().InstancePerLifetimeScope();
            builder.RegisterType<NeuroshimaHexUserStore>().As<NeuroshimaHexUserStore>();
            builder.RegisterType<NeuroshimaHexUserManager>().As<NeuroshimaHexUserManager>();

            builder.RegisterGeneric(typeof(EntityRepository<>)).As(typeof(IRepository<>));
            builder.RegisterType<UnitOfWork>().As<IUnitOfWork>().InstancePerLifetimeScope();
            builder.RegisterType<Bll.Services.Services.EmailService>().As<IEmailService>();
            builder.RegisterType<RaceService>().As<IRaceService>();
            builder.RegisterType<UserService>().As<IUserService>();
            builder.RegisterType<GameService>().As<IGameService>();
            builder.RegisterType<TeamService>().As<ITeamService>();


            builder.RegisterModule<AutomapperConfig>();

            var container = builder.Build();

            var webApiResolver = new AutofacWebApiDependencyResolver(container);
            GlobalConfiguration.Configuration.DependencyResolver = webApiResolver;

            DependencyResolver.SetResolver(new AutofacDependencyResolver(container));

            // Register the Autofac middleware FIRST, then the Autofac MVC middleware.
            app.UseAutofacMiddleware(container);
            app.UseAutofacMvc();

        }

        public static void Dispose()
        {
            _container.Dispose();
        }
    }
}