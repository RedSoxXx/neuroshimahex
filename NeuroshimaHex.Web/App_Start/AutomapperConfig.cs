﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Autofac;
using AutoMapper;
using NeuroshimaHex.Web.Mapping;

namespace NeuroshimaHex.Web.App_Start
{
    public class AutomapperConfig : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            //register all profile classes in the calling assembly
            builder.RegisterAssemblyTypes(typeof(AutomapperConfig).Assembly).As<Profile>();

            builder.Register(context => new MapperConfiguration(cfg => cfg.AddProfile<ViewModelsMappingProfile>())).AsSelf().SingleInstance();

            builder.Register(c => c.Resolve<MapperConfiguration>().CreateMapper(c.Resolve))
                .As<IMapper>()
                .InstancePerLifetimeScope();
        }
    }
}