﻿using System.Web.Mvc;

namespace NeuroshimaHex.Web.Attributes
{
    public class OverrideAuthorizeAttribute : AuthorizeAttribute
    {
        public OverrideAuthorizeAttribute(params string[] roles) : base()
        {
            Roles = string.Join(",", roles);
        }

        public override void OnAuthorization(AuthorizationContext filterContext)
        {
            base.OnAuthorization(filterContext);
        }
    }
}