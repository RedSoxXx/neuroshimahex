﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using NeuroshimaHex.Web.Models.User;

namespace NeuroshimaHex.Web.Models.Team
{
    public class TeamViewModel
    {
        public Guid Id { get; set; }
        public string TeamName { get; set; }
        public int TeammateCount { get; set; }
        public bool IsWin { get; set; }

        public Guid? GameId { get; set; }

        public List<UserViewModel> Users { get; set; }
    }
}