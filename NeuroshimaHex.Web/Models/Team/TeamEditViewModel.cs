﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace NeuroshimaHex.Web.Models.Team
{
    public class TeamEditViewModel
    {
        public Guid Id { get; set; }

        public Guid? GameId { get; set; }

        [Display(Name = "Team Name")]
        public string TeamName { get; set; }

        [Display(Name = "Teammate Count")]
        public int TeammateCount { get; set; }

    }
}