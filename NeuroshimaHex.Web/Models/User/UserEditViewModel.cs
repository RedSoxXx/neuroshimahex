﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using NeuroshimaHex.Web.Models.Race;

namespace NeuroshimaHex.Web.Models.User
{
    public class UserEditViewModel
    {
        public Guid Id { get; set; }

        [Required]
        [Display(Name = "Username")]
        [DataType(DataType.EmailAddress)]
       // [Remote("IsUsernameAvailable", "Validation", AdditionalFields = "Id")]
        public string UserName { get; set; }

        [Display(Name = "First Name")]
        public string FirstName { get; set; }

        [Display(Name = "Last Name")]
        public string LastName { get; set; }

        [Display(Name = "Email")]
        [DataType(DataType.EmailAddress)]
       // [Remote("IsEmailAvailable", "Validation", AdditionalFields = "Id")]
        public string Email { get; set; }

        [Display(Name = "Admin Access")]
        public bool IsAdmin { get; set; }

        [Display(Name = "Favorite Race")]
        public Guid? FavoriteRaceId { get; set; }

        public IEnumerable<SelectListItem> RacesSelectList { get; set; }
        public List<RaceViewModel> Races { get; set; }


        [Display(Name = "Password")]
        [DataType(DataType.Password)]
        [MinLength(6)]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm Password")]
        [System.ComponentModel.DataAnnotations.Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }

        [Display(Name = "Change Password")]
        public bool IsChangingPassword { get; set; }

        [Display(Name = "Current Password")]
        [DataType(DataType.Password)]
        [MinLength(6)]
        public string CurrentPassword { get; set; }

        [Display(Name = "New Password")]
        [DataType(DataType.Password)]
        [MinLength(6)]
        public string NewPassword { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm Password")]
        [System.ComponentModel.DataAnnotations.Compare("NewPassword", ErrorMessage = "The new password and the new password confirmation do not match.")]
        public string ConfirmNewPassword { get; set; }
    }
}