﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using NeuroshimaHex.Web.Models.Race;

namespace NeuroshimaHex.Web.Models.User
{
    public class UserViewModel
    {
        public Guid Id { get; set; }

        [Display(Name = "User Name")]
        public string UserName { get; set; }

        [Display(Name = "First Name")]
        public string FirstName { get; set; }

        [Display(Name = "Last Name")]
        public string LastName { get; set; }

        [Display(Name = "User Race")]
        public RaceViewModel Race { get; set; }
      
        public List<UsersInGameViewModel> UsersInGames { get; set; }

        public IEnumerable<SelectListItem> RacesSelectList { get; set; }
        public IEnumerable<SelectListItem> UsersSelectList { get; set; }

    }
}