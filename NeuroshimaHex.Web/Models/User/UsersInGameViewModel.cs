﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using NeuroshimaHex.Web.Models.Race;

namespace NeuroshimaHex.Web.Models.User
{
    public class UsersInGameViewModel
    {
       // public Guid Id { get; set; }
        public Guid GameId { get; set; }

        public Guid UserId { get; set; }

        public Guid RaceId { get; set; }
    }
}