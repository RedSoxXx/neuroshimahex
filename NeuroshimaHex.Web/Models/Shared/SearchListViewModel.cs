﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NeuroshimaHex.Web.Models.Shared
{
    public class SearchListViewModel<T> : BaseSearchViewModel where T : class
    {
        public IList<T> Records { get; set; }
    }
}