﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NeuroshimaHex.Web.Models.Shared
{
    public class SharedModalViewModel
    {
        public string Message { get; set; }
        public string ConfirmActionUrl { get; set; }
        public string AdditionalInformation { get; set; }
    }
}