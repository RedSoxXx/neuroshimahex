﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using NeuroshimaHex.Domain.Objects.Query;

namespace NeuroshimaHex.Web.Models.Shared
{
    public class BaseSearchViewModel
    {
        public string Search { get; set; }
        public DateTime? DateFrom { get; set; }
        public DateTime? DateTo { get; set; }
        public int Page { get; set; }
        public int PageNumber { get; set; }
        public int RecordsFiltered { get; set; }
        public int RecordsTotal { get; set; }
        public string Order { get; set; }
        public SortDirection SortDirection { get; set; }
    }
}