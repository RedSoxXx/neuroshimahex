﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using NeuroshimaHex.Web.Models.Team;

namespace NeuroshimaHex.Web.Models.Game
{
    public class SetGameStatusViewModel
    {
        public Guid Id { get; set; }

        public Guid CreatorId { get; set; }

        public DateTime? GameTime { get; set; }

        [Display(Name = "Game Name")]
        [Required]
        public string Name { get; set; }

        [Display(Name = "Is Finished")]
        [Required]
        public bool IsFinished { get; set; }

        [Display(Name = "Win Team")]
        [Required]
        public Guid WinsTeamGuid { get; set; }

        public List<TeamViewModel> Teams { get; set; }
    }
}