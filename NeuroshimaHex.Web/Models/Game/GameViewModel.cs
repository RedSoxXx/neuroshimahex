﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace NeuroshimaHex.Web.Models.Game
{
    public class GameViewModel
    {
        public Guid Id { get; set; }

        public Guid CreatorId { get; set; }

        [Display(Name = "Game Name")]
        public string Name { get; set; }

        [Display(Name = "Game Date")]
        public DateTime GameTime { get; set; }

        public bool IsFinished { get; set; }
    }
}