﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using NeuroshimaHex.Web.Models.Race;
using NeuroshimaHex.Web.Models.Team;
using NeuroshimaHex.Web.Models.User;

namespace NeuroshimaHex.Web.Models.Game
{
    public class GameEditViewModel
    {
        public Guid Id { get; set; }

        public Guid CreatorId { get; set; }

        [Display(Name = "Game Name")]
        [Required]
        public string Name { get; set; }

        [Display(Name = "Game Date")]
        public DateTime? GameTime { get; set; }

        public int GameUsersCount { get; set; }

        public bool IsFinished { get; set; }

        public List<TeamViewModel> Teams { get; set; }
        public List<UserViewModel> Users { get; set; }
        public List<RaceViewModel> Races { get; set; }
        public List<UsersInGameViewModel> UsersInGames { get; set; }
    }

}