﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using NeuroshimaHex.Web.Models.Shared;

namespace NeuroshimaHex.Web.Models.Race
{
    public class RaceStatisticModel : BaseSearchViewModel
    {
        public RaceViewModel Race { get; set; }
        public int WonCount { get; set; }
    }
}