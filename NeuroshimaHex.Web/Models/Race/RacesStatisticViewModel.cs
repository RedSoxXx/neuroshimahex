﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using NeuroshimaHex.Web.Models.Shared;

namespace NeuroshimaHex.Web.Models.Race
{
    public class RacesStatisticViewModel : SearchListViewModel<RaceViewModel>
    {
        public RacesStatisticViewModel()
        {
            Statistic = new List<RaceStatisticModel>();
        }
        public List<RaceStatisticModel> Statistic { get; set; }
    }
}