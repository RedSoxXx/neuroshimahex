﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NeuroshimaHex.Web.Models.Race
{
    public class RaceInfoViewModel
    {
        public List<RaceViewModel> Races { get; set; }
    }
}