﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NeuroshimaHex.Web.Models
{
    public class NavigationItems
    {
        public const string Races = "Races";
        public const string Users = "Users";
    }
}