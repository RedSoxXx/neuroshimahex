﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Security.Principal;
using System.Web;
using Microsoft.AspNet.Identity;

namespace NeuroshimaHex.Web.Extensions
{
    public static class IdentityExtensions
    {
        public static Guid GetUserIdGuid(this IIdentity identity)
        {
            Guid result;
            return Guid.TryParse(identity.GetUserId<string>(), out result) ? result : Guid.Empty;
        }

        public static string GetContactName(this IIdentity identity)
        {
            var claimsIdentity = identity as ClaimsIdentity;

            if (claimsIdentity != null)
            {
                var cId = claimsIdentity.Claims.SingleOrDefault(x => x.Type == "ContactName");
                if (!string.IsNullOrEmpty(cId.Value))
                {
                    return cId.Value;
                }

            }

            return string.Empty;
        }

        public static Guid? GetCompanyHierarchyId(this IIdentity identity)
        {
            var claimsIdentity = identity as ClaimsIdentity;

            if (claimsIdentity != null)
            {
                var cHierarchyId = claimsIdentity.Claims.SingleOrDefault(x => x.Type == "CompanyHierarchyId");
                if (cHierarchyId != null && !string.IsNullOrEmpty(cHierarchyId.Value))
                {
                    return Guid.Parse(cHierarchyId.Value);
                }

            }

            return null;
        }
    }
}