﻿using Microsoft.Owin;
using NeuroshimaHex.Web.App_Start;
using Owin;

[assembly: OwinStartupAttribute(typeof(NeuroshimaHex.Web.Startup))]
namespace NeuroshimaHex.Web
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
            AutofacConfig.RegisterComponents(app);
        }
    }
}
