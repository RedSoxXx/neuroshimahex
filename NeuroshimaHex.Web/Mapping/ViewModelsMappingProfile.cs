﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AutoMapper;
using NeuroshimaHex.Domain.Entities;
using NeuroshimaHex.Lib.Extentions;
using NeuroshimaHex.Web.Models.Game;
using NeuroshimaHex.Web.Models.Race;
using NeuroshimaHex.Web.Models.Team;
using NeuroshimaHex.Web.Models.User;
using GameViewModel = NeuroshimaHex.Web.Models.Game.GameViewModel;

namespace NeuroshimaHex.Web.Mapping
{
    public class ViewModelsMappingProfile : Profile
    {
        protected override void Configure()
        {
            CreateMap<Race, RaceEditViewModel>().IgnoreAllNonExisting().ReverseMap().IgnoreAllNonExisting();
            CreateMap<Race, RaceViewModel>().IgnoreAllNonExisting().ReverseMap().IgnoreAllNonExisting();
            CreateMap<RaceViewModel, Race>().IgnoreAllNonExisting().ReverseMap().IgnoreAllNonExisting();
            CreateMap<RaceEditViewModel, Race>().IgnoreAllNonExisting().ReverseMap().IgnoreAllNonExisting();

            CreateMap<User, UserEditViewModel>().IgnoreAllNonExisting().ReverseMap().IgnoreAllNonExisting();
            CreateMap<UserEditViewModel, User>().IgnoreAllNonExisting().ReverseMap().IgnoreAllNonExisting();
            CreateMap<User, UserViewModel>().IgnoreAllNonExisting().ReverseMap().IgnoreAllNonExisting();
            CreateMap<UserViewModel, User>().IgnoreAllNonExisting().ReverseMap().IgnoreAllNonExisting();

            CreateMap<Game, GameEditViewModel>().IgnoreAllNonExisting().ReverseMap().IgnoreAllNonExisting();
            CreateMap<GameEditViewModel, Game>().IgnoreAllNonExisting().ReverseMap().IgnoreAllNonExisting();
            CreateMap<Game, GameViewModel>().IgnoreAllNonExisting().ReverseMap().IgnoreAllNonExisting();
            CreateMap<GameViewModel, Game>().IgnoreAllNonExisting().ReverseMap().IgnoreAllNonExisting();
            CreateMap<Game, SetGameStatusViewModel>().IgnoreAllNonExisting().ReverseMap().IgnoreAllNonExisting();
            CreateMap<SetGameStatusViewModel, Game > ().IgnoreAllNonExisting().ReverseMap().IgnoreAllNonExisting();

            CreateMap<Team, TeamEditViewModel>().IgnoreAllNonExisting().ReverseMap().IgnoreAllNonExisting();
            CreateMap<Team, TeamViewModel>().IgnoreAllNonExisting().ReverseMap().IgnoreAllNonExisting();
            CreateMap<TeamViewModel, Team>().IgnoreAllNonExisting().ReverseMap().IgnoreAllNonExisting();
            CreateMap<TeamEditViewModel, Team>().IgnoreAllNonExisting().ReverseMap().IgnoreAllNonExisting();

            CreateMap<UsersInGame, UsersInGameViewModel>().IgnoreAllNonExisting().ReverseMap().IgnoreAllNonExisting();
            CreateMap<UsersInGameViewModel, UsersInGame>().IgnoreAllNonExisting().ReverseMap().IgnoreAllNonExisting();
        }
    }
}