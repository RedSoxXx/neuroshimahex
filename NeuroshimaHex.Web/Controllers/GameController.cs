﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using AutoMapper;
using NeuroshimaHex.Bll.Contracts.Services;
using NeuroshimaHex.Bll.Services.Identity;
using NeuroshimaHex.Domain.Entities;
using NeuroshimaHex.Domain.Objects.Query;
using NeuroshimaHex.Web.Models.Game;
using NeuroshimaHex.Web.Models.Race;
using NeuroshimaHex.Web.Models.Team;
using NeuroshimaHex.Web.Models.User;

namespace NeuroshimaHex.Web.Controllers
{
    public class GameController : Controller
    {
        private readonly IMapper _mapper;
        private readonly IGameService _gameService;
        private readonly ITeamService _teamService;
        private readonly IUserService _userService;
        private readonly IRaceService _raceService;
        private readonly NeuroshimaHexUserManager _userManager;

        public GameController(IMapper mapper, IGameService gameService, ITeamService teamService, IUserService userService, IRaceService raceService, NeuroshimaHexUserManager userManager)
        {
            _mapper = mapper;
            _gameService = gameService;
            _teamService = teamService;
            _userService = userService;
            _userManager = userManager;
            _raceService = raceService;
        }

        public ActionResult Index(DateTime? dateFrom, DateTime? dateTo, string search = "", int page = 1, int length = 15)
        {
            var result = _gameService.GetGames(new SearchQuery
            {
                Orderby = new ColumnOrder
                {
                    Column = "Name",
                    Direction = SortDirection.Asc
                },
                Paging = new Paging
                {
                    Length = length,
                    Skip = (page - 1) * length,
                },
                Search = search,
                DateFrom = dateFrom,
                DateTo = dateTo
            });
            return View(new GameListViewModel()
            {
                Search = search,
                DateFrom = dateFrom,
                DateTo = dateTo,
                Page = page,
                RecordsFiltered = result.RecordsFiltered,
                RecordsTotal = result.RecordsTotal,
                PageNumber = result.RecordsFiltered / length + (result.RecordsFiltered % length > 0 ? 1 : 0),
                Records = result.Data.Select(x => _mapper.Map<GameViewModel>(x)).ToList()
            });
        }

        public ActionResult Create()
        {
            var model = new GameEditViewModel();
            model.Teams = new List<TeamViewModel>();
            ViewBag.DefaultTeamName = "Game #" + _gameService.GetGameCount();
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(GameEditViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            model.Id = Guid.NewGuid();
            model.UsersInGames = new List<UsersInGameViewModel>();

            if (model.CreatorId == Guid.Empty)
            {
                model.CreatorId = _userManager.FindByNameAsync(User.Identity.Name).Result.Id;
            }

            var users = _userService.GetUsers();
            foreach (var team in model.Teams)
            {
                foreach (var teamUser in team.Users)
                {
                    var userInGame = new UsersInGameViewModel()
                    {
                        GameId = model.Id,
                        UserId = teamUser.Id,
                        RaceId = teamUser.Race.Id
                    };
                    //userInGame.Id = Guid.NewGuid();
                    model.UsersInGames.Add(userInGame);
                }
            }

            foreach (var team in model.Teams)
            {
                team.Id = new Guid();
                team.GameId = model.Id;
                team.TeammateCount = team.Users.Count;
            }

            var value = _mapper.Map<GameEditViewModel, Game>(model);
            foreach (var team in value.Teams)
            {
                var currentTeamUsers =
                    team.Users.Select(teamUser => teamUser.Id)
                        .Select(userId => users.Single(x => x.Id == userId))
                        .ToList();
                team.Users = currentTeamUsers;
            }
            _gameService.CreateGame(value);

            return RedirectToAction("Index");
        }

        public ActionResult Edit(Guid id)
        {
            var value = _gameService.GetGameById(id);
            var model = _mapper.Map<GameEditViewModel>(value);
            var races = _raceService.GetRaces();
            model.Races = _mapper.Map<List<RaceViewModel>>(races);
            var users = _userService.GetUsers();
            model.Users = _mapper.Map<List<UserViewModel>>(users);
            foreach (var teamViewModel in model.Teams)
            {
                foreach (var user in teamViewModel.Users)
                {
                    var userRaceId = model.UsersInGames.Single(u => u.UserId == user.Id).RaceId;
                    var racesListItem = model.Races.Select(r => new SelectListItem
                    {
                        Text = r.Name,
                        Value = r.Id.ToString(),
                        Selected = r.Id == userRaceId
                    }).ToList();
                    user.RacesSelectList = racesListItem;
                    var usersListItem = model.Users.Select(u => new SelectListItem
                    {
                        Text = u.FirstName + " " + u.LastName,
                        Value = u.Id.ToString(),
                        Selected = u.Id == user.Id
                    }).ToList();
                    user.UsersSelectList = usersListItem;
                }
            }
            return View("~/Views/Game/Edit.cshtml",model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(GameEditViewModel model)
        {
            var value = _gameService.GetGameById(model.Id);
            var listTeams = new List<Team>();
            foreach (var team in model.Teams)
            {
                listTeams.Add(new Team
                {
                    GameId = model.Id,
                    Id = team.Id,
                    TeamName = team.TeamName,
                    TeammateCount = team.TeammateCount

                });
                team.GameId = model.Id;
            }
            _teamService.UpdateTeam(listTeams);
            if (value != null)
            {
                value.GameTime = model.GameTime.Value;
                value.Name = model.Name;
                _gameService.UpdateGame(value);
            }
            return RedirectToAction("Index");
        }

        public ActionResult Remove(GameEditViewModel model)
        {
            var value = _gameService.GetGameById(model.Id);
            if (value != null)
            {
                //_mapper.Map(model, value);
                _gameService.ToArchive(value);
            }
            return RedirectToAction("Index");
        }

        public ActionResult GameInfo(Guid id)
        {
            var value = _gameService.GetGameById(id);
            var model = _mapper.Map<GameEditViewModel>(value);
            var races = _raceService.GetRaces();
            model.Races = _mapper.Map<List<RaceViewModel>>(races);
            var users = _userService.GetUsers();
            model.Users = _mapper.Map<List<UserViewModel>>(users);

            return View("~/Views/Game/Info.cshtml", model);
        }

        public ActionResult SetGameStatus(Guid id)
        {
            var value = _gameService.GetGameById(id);
            var model = _mapper.Map<SetGameStatusViewModel>(value);

            return View("~/Views/Game/SetStatus.cshtml", model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult SetGameStatus(SetGameStatusViewModel model)
        {
            var value = _gameService.GetGameById(model.Id);
            value.Teams.Single(x => x.Id == model.WinsTeamGuid).IsWin = true;
            value.IsFinished = model.IsFinished;
            _gameService.UpdateGame(value);
            return RedirectToAction("Index");
        }


    }
}