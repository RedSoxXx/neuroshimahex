﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AutoMapper;
using Microsoft.AspNet.Identity;
using NeuroshimaHex.Bll.Contracts.Services;
using NeuroshimaHex.Bll.Services.Identity;
using NeuroshimaHex.Domain.Entities;
using NeuroshimaHex.Domain.Enums;
using NeuroshimaHex.Domain.Objects.Query;
using NeuroshimaHex.Web.Attributes;
using NeuroshimaHex.Web.Extensions;
using NeuroshimaHex.Web.Models.Race;
using NeuroshimaHex.Web.Models.User;

namespace NeuroshimaHex.Web.Controllers
{
    public class UserController : Controller
    {
        private readonly IMapper _mapper;
        private readonly IUserService _userService;
        private readonly IRaceService _raceService;
        private readonly NeuroshimaHexUserManager _userManager;


        public UserController(IMapper mapper, IUserService userService, IRaceService raceService, NeuroshimaHexUserManager userManager)
        {
            _mapper = mapper;
            _userService = userService;
            _raceService = raceService;
            _userManager = userManager;
        }

        public ActionResult GetUsers(string search = "", int page = 1, int length = 15)
        {
            var result = _userService.GetUsers(new SearchQuery
            {
                Orderby = new ColumnOrder
                {
                    Column = "UserName",
                    Direction = SortDirection.Asc
                },
                Paging = new Paging
                {
                    Length = length,
                    Skip = (page - 1) * length,
                },
                Search = search,
            });
            return View("~/Views/User/GetUsers.cshtml",new UserListViewModel()
            {
                Search = search,
                Page = page,
                RecordsFiltered = result.RecordsFiltered,
                RecordsTotal = result.RecordsTotal,
                PageNumber = result.RecordsFiltered / length + (result.RecordsFiltered % length > 0 ? 1 : 0),
                Records = result.Data.Select(x => _mapper.Map<UserViewModel>(x)).ToList()
            });
        }

        public ActionResult Create()
        {
            var model = new UserEditViewModel();
            var races = _raceService.GetRaces();
            model.Races = _mapper.Map<List<RaceViewModel>>(races);
            var defaultSelectItem = new SelectListItem { Text = "Select", Value = "0", Selected = true, Disabled = true };
            var racesListItem = model.Races.Select(r => new SelectListItem
            {
                Text = r.Name,
                Value = r.Id.ToString(),
            }).ToList();
            racesListItem.Add(defaultSelectItem);
            model.RacesSelectList = racesListItem;
            return View("~/Views/User/Create.cshtml",model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(UserEditViewModel model)
        {
            var user = _userManager.FindById(model.Id);
            if (user == null)
            {
                user = _mapper.Map<User>(model);
                user.CreatedAt = DateTime.UtcNow;
                user.LastModifiedAt = DateTime.UtcNow;
                var result = _userManager.Create(user, model.Password);

                if (result.Succeeded && model.IsAdmin)
                {
                    _userManager.AddToRole(user.Id, UserRoles.Admin);
                }
            }
            return RedirectToAction("GetUsers");
        }

        [OverrideAuthorize()]
        public ActionResult Edit(Guid id, string returnUrl = null)
        {
            ViewBag.ReturnUrl = returnUrl;
            var value = _userService.GetById(id);
            ViewBag.ReturnUrl = returnUrl == null ? Url.Action("GetUsers", "User") : returnUrl;
            var model = _mapper.Map<UserEditViewModel>(value);
            model.IsAdmin = _userService.IsAdmin(id);

            var races = _raceService.GetRaces();
            model.Races = _mapper.Map<List<RaceViewModel>>(races);
            var emptySelectItem = new SelectListItem { Text = "  ", Selected = !model.FavoriteRaceId.HasValue };
            var racesListItem = model.Races.Select(r => new SelectListItem
            {
                Text = r.Name,
                Value = r.Id.ToString(),
                Selected = r.Id == model.FavoriteRaceId
            }).ToList();
            racesListItem.Add(emptySelectItem);
            model.RacesSelectList = racesListItem;

            return View("~/Views/User/Edit.cshtml", model);
        }

        public ActionResult Archive(Guid id)
        {
            _userService.ToArchive(id);
            return RedirectToAction("GetUsers");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(UserEditViewModel model, string returnUrl = null)
        {
            var user = _userService.GetById(model.Id);
            if (user == null || !ModelState.IsValid)
                return View(model);

            _mapper.Map(model, user);
            _userService.Update(user);

            if (model.IsAdmin)
            {
                _userService.AddRole(user, UserRoles.Admin);
            }
            else
            {
                _userService.RemoveRole(user, UserRoles.Admin);
            }

            if (model.IsChangingPassword)
            {
                var result = _userManager.ChangePassword(user.Id, model.CurrentPassword, model.NewPassword);
                if (!result.Succeeded)
                {
                    ModelState.AddModelError("CurrentPassword", string.Join(",", result.Errors));
                    return View(model);
                }
            }

            if (returnUrl != null)
                return Redirect(returnUrl);

            return RedirectToAction("GetUsers");
        }

    }
}