﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AutoMapper;
using NeuroshimaHex.Bll.Contracts.Services;
using NeuroshimaHex.Bll.Services.Identity;
using NeuroshimaHex.Web.Models.Game;
using NeuroshimaHex.Web.Models.Race;
using NeuroshimaHex.Web.Models.Team;
using NeuroshimaHex.Web.Models.User;

namespace NeuroshimaHex.Web.Controllers
{
    public class TeamController : Controller
    {
        private readonly IMapper _mapper;
        private readonly IUserService _userService;
        private readonly IRaceService _raceService;
        private readonly NeuroshimaHexUserManager _userManager;


        public TeamController(IMapper mapper, IUserService userService, IRaceService raceService, NeuroshimaHexUserManager userManager)
        {
            _mapper = mapper;
            _userService = userService;
            _userManager = userManager;
            _raceService = raceService;
        }

        public PartialViewResult CreateTeam(int teamCount = 2, int teammateCount = 2)
        {
            var model = new GameEditViewModel();
            model.Teams = new List<TeamViewModel>();
            var users = _userService.GetUsers();
            model.Users = _mapper.Map<List<UserViewModel>>(users);

            var usersCount = model.Users.Count;
            if (teamCount > usersCount)
            {
                teamCount = usersCount;
            }

            if (teammateCount * teamCount > usersCount)
            {
                teammateCount = usersCount / teamCount;
            }
            model.GameUsersCount = teamCount * teammateCount;
            var races = _raceService.GetRaces();
            model.Races = _mapper.Map<List<RaceViewModel>>(races);

            var defaultSelectItem = new SelectListItem { Text = "Select", Value = "0", Selected = true, Disabled = true };
            var racesListItem = model.Races.Select(r => new SelectListItem
            {
                Text = r.Name,
                Value = r.Id.ToString()
            }).ToList();
            racesListItem.Add(defaultSelectItem);

            var usersListItem = model.Users.Select(u => new SelectListItem
            {
                Text = u.FirstName + " " + u.LastName,
                Value = u.Id.ToString()
            }).ToList();
            usersListItem.Add(defaultSelectItem);


            for (var i = 0; i < teamCount; i++)
            {
                var team = new TeamViewModel();
                team.Users = new List<UserViewModel>();
                for (var j = 0; j < teammateCount; j++)
                {
                    team.TeamName = "Team #" + (i + 1);
                    team.Users.Add(new UserViewModel
                    {
                        RacesSelectList = racesListItem,
                        UsersSelectList = usersListItem
                    });
                }
                team.TeammateCount = teammateCount;
                model.Teams.Add(team);
            }
            model.UsersInGames = new List<UsersInGameViewModel>();

            return PartialView("~/Views/Team/_CommonTeamEdit.cshtml", model);
        }
    }
}