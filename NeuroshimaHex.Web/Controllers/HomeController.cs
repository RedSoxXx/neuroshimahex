﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using NeuroshimaHex.Bll.Services.Identity;
using NeuroshimaHex.Domain.Enums;
using NeuroshimaHex.Web.Extensions;

namespace NeuroshimaHex.Web.Controllers
{
    [Authorize]
    public class HomeController : Controller
    {
        private readonly NeuroshimaHexUserManager _userManager;

        public HomeController(NeuroshimaHexUserManager userManager)
        {
            _userManager = userManager;
        }
        public async Task<ActionResult> Index()
        {
            var userId = User.Identity.GetUserIdGuid();
            var isAdmin = await _userManager.IsInRoleAsync(userId, UserRoles.Admin);
            if (isAdmin)
            {
                return RedirectToAction("Index", "Race");
            }
            return RedirectToAction("RaceInfo", "Race");
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}