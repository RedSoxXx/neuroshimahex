﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AutoMapper;
using NeuroshimaHex.Bll.Contracts.Services;
using NeuroshimaHex.Domain.Entities;
using NeuroshimaHex.Domain.Objects.Query;
using NeuroshimaHex.Web.Models.Race;

namespace NeuroshimaHex.Web.Controllers
{
    public class RaceController : Controller
    {
        private readonly IMapper _mapper;
        private readonly IRaceService _raceService;

        public RaceController(IMapper mapper, IRaceService raceService)
        {
            _mapper = mapper;
            _raceService = raceService;
        }

        public ActionResult RaceInfo()
        {
            var model = new RaceInfoViewModel();
            var races = _raceService.GetRaces();
            model.Races = _mapper.Map<List<RaceViewModel>>(races);
            return View("~/Views/Race/RaceInfo.cshtml", model);
        }

        public ActionResult Index(string search = "", int page = 1, int length = 15)
        {
            var result = _raceService.GetRaces(new SearchQuery
            {
                Orderby = new ColumnOrder
                {
                    Column = "Name",
                    Direction = SortDirection.Asc
                },
                Paging = new Paging
                {
                    Length = length,
                    Skip = (page - 1) * length,
                },
                Search = search,
            });
            return View(new RaceListViewModel()
            {
                Search = search,
                Page = page,
                RecordsFiltered = result.RecordsFiltered,
                RecordsTotal = result.RecordsTotal,
                PageNumber = result.RecordsFiltered / length + (result.RecordsFiltered % length > 0 ? 1 : 0),
                Records = result.Data.Select(x => _mapper.Map<RaceViewModel>(x)).ToList()
            });
        }

        public ActionResult Create()
        {
            var model = new RaceEditViewModel();
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(RaceEditViewModel model)
        {

            if (ModelState.IsValid)
            {
                model.Id = Guid.NewGuid();
                if (model.Image == null)
                {
                    string imageFile = Path.Combine(Server.MapPath("~/Images"), "orc_default.png");
                    byte[] buffer = System.IO.File.ReadAllBytes(imageFile);
                    model.Image = buffer;
                }
                var value = _mapper.Map<Race>(model);
                _raceService.AddRace(value);
                
                return RedirectToAction("Index");
            }
            return View(model);
        }

        public ActionResult Edit(Guid id)
        {
            var value = _raceService.GetRaceById(id);
            var model = _mapper.Map<RaceEditViewModel>(value);
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(RaceEditViewModel model)
        {
            var value = _raceService.GetRaceById(model.Id);
            if (value != null)
            {
                _mapper.Map(model, value);
                _raceService.UpdateRace(value);
            }
            return RedirectToAction("Index");
        }

        public ActionResult Remove(RaceEditViewModel model)
        {
            var value = _raceService.GetRaceById(model.Id);
            if (value != null)
            {
                _raceService.ToArchive(value);
            }
            return RedirectToAction("Index");
        }

        public ActionResult Statistic(DateTime? dateFrom, DateTime? dateTo)
        {
            var model = new RacesStatisticViewModel();
            var wonRacesId = _raceService.GetWonRacesId(new SearchQuery
            {
                DateFrom = dateFrom,
                DateTo = dateTo
            });
            var values = _raceService.GetRaces();
            var races = _mapper.Map<List<RaceViewModel>>(values);
            foreach (var raceid in wonRacesId.Distinct())
            {
                var race = races.Single(r => r.Id == raceid);
                var wonCount = wonRacesId.Count(x => x == raceid);
                model.Statistic.Add(new RaceStatisticModel { Race = race, WonCount = wonCount });

            }
            return View("~/Views/Race/Statistic.cshtml",model);
        }
    }
}