﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using AutoMapper;
using NeuroshimaHex.Bll.Contracts.Services;
using NeuroshimaHex.Bll.Services.Identity;
using NeuroshimaHex.Domain.Objects.Query;
using NeuroshimaHex.Web.Models.Game;

namespace NeuroshimaHex.Web.Controllers
{
    public class GameApiController : ApiController
    {
        private readonly IMapper _mapper;
        private readonly IGameService _gameService;
        private readonly ITeamService _teamService;
        private readonly IUserService _userService;
        private readonly IRaceService _raceService;
        private readonly NeuroshimaHexUserManager _userManager;

        public GameApiController(IMapper mapper, IGameService gameService, ITeamService teamService, IUserService userService, IRaceService raceService, NeuroshimaHexUserManager userManager)
        {
            _mapper = mapper;
            _gameService = gameService;
            _teamService = teamService;
            _userService = userService;
            _userManager = userManager;
            _raceService = raceService;
        }

        // GET api/<controller>
        [Route("api/gameapi/{dateFrom}/{dateTo}/{search}/{page}/{length}")]
        public IEnumerable<GameViewModel> Get(DateTime? dateFrom = null, DateTime? dateTo = null, string search = "", int page = 1, int length = 15)
        {
            var result = _gameService.GetGames(new SearchQuery
            {
                Orderby = new ColumnOrder
                {
                    Column = "Name",
                    Direction = SortDirection.Asc
                },
                Paging = new Paging
                {
                    Length = length,
                    Skip = (page - 1) * length,
                },
                Search = search,
                DateFrom = dateFrom,
                DateTo = dateTo
            });
            return result.Data.Select(x => _mapper.Map<GameViewModel>(x)).ToList();
        }

        // GET api/<controller>/5
        public string Get(int id)
        {
            return "value";
        }

        // POST api/<controller>
        public void Post([FromBody]string value)
        {
        }

        // PUT api/<controller>/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/<controller>/5
        public void Delete(int id)
        {
        }
    }
}