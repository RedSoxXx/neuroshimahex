﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NeuroshimaHex.Bll.Contracts.Repositories;

namespace NeuroshimaHex.Bll.Contracts
{
    public interface IUnitOfWork
    {
        IRepository<T> GetRepository<T>() where T : class;
        Task<int> CommitAsync();
        int Commit();
        bool AutoDetectChanges { get; set; }
        List<T> ExecuteStoredProcedure<T>(string procedureName);
    }
}
