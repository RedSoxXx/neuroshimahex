﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NeuroshimaHex.Domain.Entities;
using NeuroshimaHex.Domain.Objects.Query;

namespace NeuroshimaHex.Bll.Contracts.Services
{
    public interface IRaceService
    {
        Race GetRaceById(Guid id);
        QueryResponse<Race> GetRaces(SearchQuery searchQuery);
        List<Race> GetRaces();
        void AddRace(Race race);
        void UpdateRace(Race race);
        void RemoveRace(Race race);
        void ToArchive(Race race);
        List<Guid> GetWonRacesId(SearchQuery searchQuery);

    }
}
