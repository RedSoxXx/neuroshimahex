﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NeuroshimaHex.Bll.Contracts.Services
{
    public interface IEmailService
    {
        void SendEmail(string emailTo, string subject, string text);
        void SendEmail(string emailTo, string subject, string body, string from, string displayFrom, bool isHtml = true);
        void SendEmail(IEnumerable<string> recipients, string subject, string text);
        void SendEmail(IEnumerable<string> recipients, string subject, string body, string from, string displayFrom, bool isHtml = true);
    }
}
