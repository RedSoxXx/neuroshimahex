﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NeuroshimaHex.Domain.Entities;
using NeuroshimaHex.Domain.Objects.Query;

namespace NeuroshimaHex.Bll.Contracts.Services
{
    public interface IGameService
    {
        Game GetGameById(Guid id);
        void CreateGame(Game game);
        void UpdateGame(Game game);
        void RemoveGame(Game game);
        void ToArchive(Game game);
        QueryResponse<Game> GetGames(SearchQuery searchQuery);
        int GetGameCount();
    }
}
