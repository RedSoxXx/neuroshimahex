﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NeuroshimaHex.Domain.Entities;
using NeuroshimaHex.Domain.Objects.Query;

namespace NeuroshimaHex.Bll.Contracts.Services
{
    public interface IUserService
    {
        User GetById(Guid id);
        void Update(User user);
        void ToArchive(Guid id);
        void Delete(User user);
        void Delete(Guid id);
        QueryResponse<User> GetUsers(SearchQuery searchQuery);
        List<User> GetUsers();
        Role GetRole(string name);
        bool IsAdmin(Guid userId);
        void AddRole(User user, string roleName);
        void RemoveRole(User user, string roleName);

    }
}
