﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NeuroshimaHex.Domain.Entities;
using NeuroshimaHex.Domain.Objects.Query;

namespace NeuroshimaHex.Bll.Contracts.Services
{
    public interface ITeamService
    {
        Team GetTeamById(Guid id);
        void CreateTeam(Team game);
        void UpdateTeam(Team game);
        void RemoveTeam(Team game);
        QueryResponse<Team> GetTeams(SearchQuery searchQuery);
        void UpdateTeam(List<Team> listTeams);
    }
}
