﻿using AutoMapper;

namespace NeuroshimaHex.Lib.Extentions
{
    public static class MappingExtensions
    {
        /// <summary>
        /// Method of ignor NotMapped entity's fields
        /// </summary>
        /// <typeparam name="TSource">Source entity</typeparam>
        /// <typeparam name="TDestination">Destination entity</typeparam>
        /// <param name="expression">Expression</param>
        /// <returns>Expression</returns>
        // from http://stackoverflow.com/a/35376631
        public static IMappingExpression<TSource, TDestination> IgnoreAllNonExisting<TSource, TDestination>(this IMappingExpression<TSource, TDestination> expression)
        {
            foreach (var property in expression.TypeMap.GetUnmappedPropertyNames())
            {
                expression.ForMember(property, opt => opt.Ignore());
            }
            return expression;
        }
    }
}
