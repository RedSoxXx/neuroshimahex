﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NeuroshimaHex.Domain.Entities;

namespace NeuroshimaHex.Persistence.Mapping
{
    public class TeamMap : EntityTypeConfiguration<Team>
    {
        public TeamMap()
        {
            this.HasKey(r => r.Id);

            this.Property(r => r.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            this.HasMany<User>(s => s.Users)
                .WithMany(c => c.Teams)
                .Map(cs =>
                {
                    cs.MapLeftKey("TeamRefId");
                    cs.MapRightKey("UserRefId");
                    cs.ToTable("UserTeam");
                });
        }
    }
}
