﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NeuroshimaHex.Domain.Entities;

namespace NeuroshimaHex.Persistence.Mapping
{
    public class RaceMap : EntityTypeConfiguration<Race>
    {
        public RaceMap()
        {
            this.HasKey(r => r.Id);

            this.Property(r => r.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            this.HasMany(x => x.UsersInGames).WithRequired(x => x.Race).HasForeignKey(x => x.RaceId);
            this.HasMany(x => x.Users).WithOptional(x => x.Race).HasForeignKey(x => x.FavoriteRaceId).WillCascadeOnDelete(false);
        }
    }
}
