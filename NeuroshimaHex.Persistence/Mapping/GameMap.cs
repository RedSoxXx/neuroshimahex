﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NeuroshimaHex.Domain.Entities;

namespace NeuroshimaHex.Persistence.Mapping
{
    public class GameMap : EntityTypeConfiguration<Game>
    {
        public GameMap()
        {
            this.HasKey(r => r.Id);

            this.Property(r => r.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            this.HasMany(x => x.Teams).WithRequired(x => x.Game).HasForeignKey(x => x.GameId);
            this.HasMany(x => x.UsersInGames).WithRequired(x => x.Game).HasForeignKey(x => x.GameId);
        }
    }
}
