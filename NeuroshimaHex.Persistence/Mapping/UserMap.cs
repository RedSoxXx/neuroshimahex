﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NeuroshimaHex.Domain.Entities;

namespace NeuroshimaHex.Persistence.Mapping
{
    public class UserMap : EntityTypeConfiguration<User>
    {
        public UserMap()
        {
            this.HasKey(u => u.Id);

            this.Property(u => u.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            this.HasMany(x => x.UsersInGames).WithRequired(x => x.User).HasForeignKey(x => x.UserId);

            this.HasMany(x => x.Games).WithRequired(x => x.User).HasForeignKey(x => x.CreatorId).WillCascadeOnDelete(false);
        }
    }
}
