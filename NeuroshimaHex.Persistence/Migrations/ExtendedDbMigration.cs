﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace NeuroshimaHex.Persistence.Migrations
{
    public abstract class ExtendedDbMigration : DbMigration
    {
        internal void SqlFile(string scriptFileName)
        {
            using (var sqlStream = Assembly.GetExecutingAssembly().GetManifestResourceStream("NeuroshimaHex.Persistence.Migrations.SQLFiles." + scriptFileName))
            using (var sqlStreamReader = new StreamReader(sqlStream))
            {
                string sqlScript = sqlStreamReader.ReadToEnd();
                sqlScript = sqlScript.Replace("'", "''");
                var parts = sqlScript.Split(new[] { "--- GO ---" }, StringSplitOptions.RemoveEmptyEntries).Where(p => !string.IsNullOrWhiteSpace(p));
                foreach (var part in parts)
                {
                    Sql("EXEC dbo.sp_executesql @statement = N'" + part + "'");
                }
            }
        }

        protected string UniqueIndexMultippleNulls(string tableName, string columnName, string indexName)
        {
            return string.Format(@"
                CREATE UNIQUE NONCLUSTERED INDEX {0}
                ON {1}({2}) 
                WHERE {2} IS NOT NULL;",
                indexName, tableName, columnName);
        }
    }
}
