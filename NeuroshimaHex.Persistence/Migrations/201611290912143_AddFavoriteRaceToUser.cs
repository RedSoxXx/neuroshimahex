namespace NeuroshimaHex.Persistence.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddFavoriteRaceToUser : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Users", "FavoriteRaceId", c => c.Guid(nullable: true));
            CreateIndex("dbo.Users", "FavoriteRaceId");
            AddForeignKey("dbo.Users", "FavoriteRaceId", "dbo.Races", "Id", cascadeDelete: false);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Users", "FavoriteRaceId", "dbo.Races");
            DropIndex("dbo.Users", new[] { "FavoriteRaceId" });
            DropColumn("dbo.Users", "FavoriteRaceId");
        }
    }
}
