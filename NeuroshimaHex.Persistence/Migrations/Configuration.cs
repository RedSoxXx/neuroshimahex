using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using NeuroshimaHex.Domain.Entities;
using NeuroshimaHex.Domain.Enums;
using NeuroshimaHex.Domain.Identity;

namespace NeuroshimaHex.Persistence.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    public class Configuration : DbMigrationsConfiguration<NeuroshimaHex.Persistence.NeuroshimaHexContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
        }

        protected override void Seed(NeuroshimaHex.Persistence.NeuroshimaHexContext context)
        {
            var roleStore = new RoleStore<Role, Guid, AspnetUserRole>(context);
            var roleManager = new RoleManager<Role, Guid>(roleStore);

            if (!roleManager.RoleExists(UserRoles.Admin))
            {
                roleManager.Create(new Role { Id = new Guid("0791A1C6-6598-45C7-B7DB-5EE7DED7F786"), Name = UserRoles.Admin });
            }

            if (!roleManager.RoleExists(UserRoles.User))
            {
                roleManager.Create(new Role { Id = new Guid("50127D29-AA1A-437C-92E8-6F0AF58B2208"), Name = UserRoles.User });
            }

            var userStore = new UserStore<User, Role, Guid, AspnetUserLogin, AspnetUserRole, AspnetUserClaim>(context);
            var userManager = new UserManager<User, Guid>(userStore);

            if (!context.Users.Any(x => x.Email == "admin@admin.com"))
            {
                var superAdmin = new User()
                {
                    FirstName = "Admin",
                    LastName = "Admin",
                    Email = "admin@admin.com",
                    UserName = "admin@admin.com",
                    CreatedAt = DateTime.UtcNow,
                    LastModifiedAt = DateTime.UtcNow,
                    IsArchive = false,
                    FavoriteRaceId = null
                };

                userManager.Create(superAdmin, "Qq!123");
                userManager.AddToRole(superAdmin.Id, UserRoles.Admin);
            }

            if (!context.Users.Any(x => x.Email == "user@user.com"))
            {
                var superAdmin = new User()
                {
                    FirstName = "User",
                    LastName = "User",
                    Email = "user@user.com",
                    UserName = "user@user.com",
                    CreatedAt = DateTime.UtcNow,
                    LastModifiedAt = DateTime.UtcNow,
                    IsArchive = false,
                    FavoriteRaceId = null
                };

                userManager.Create(superAdmin, "Qq!123");
                userManager.AddToRole(superAdmin.Id, UserRoles.User);
            }
        }
    }
}
