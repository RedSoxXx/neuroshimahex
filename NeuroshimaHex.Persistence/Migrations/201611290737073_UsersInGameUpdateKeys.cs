namespace NeuroshimaHex.Persistence.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UsersInGameUpdateKeys : ExtendedDbMigration
    {
        public override void Up()
        {
            DropPrimaryKey("dbo.UsersInGames");
            AddPrimaryKey("dbo.UsersInGames", new[] { "GameId", "UserId" });
            SqlFile("201611290737073_UsersInGameUpdateKeys.sql");
        }
        
        public override void Down()
        {
            DropPrimaryKey("dbo.UsersInGames");
            AddPrimaryKey("dbo.UsersInGames", "Id");
        }
    }
}
