namespace NeuroshimaHex.Persistence.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RemovPKUsersInGame : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.UsersInGames", "Id");
        }
        
        public override void Down()
        {
            AddColumn("dbo.UsersInGames", "Id", c => c.Guid(nullable: false));
        }
    }
}
