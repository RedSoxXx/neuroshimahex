namespace NeuroshimaHex.Persistence.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddIsArchiveUser : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Users", "IsArchive", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Users", "IsArchive");
        }
    }
}
