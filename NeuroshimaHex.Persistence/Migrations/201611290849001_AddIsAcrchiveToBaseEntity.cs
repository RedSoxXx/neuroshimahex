namespace NeuroshimaHex.Persistence.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddIsAcrchiveToBaseEntity : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Games", "IsArchive", c => c.Boolean(nullable: false));
            AddColumn("dbo.Teams", "IsArchive", c => c.Boolean(nullable: false));
            AddColumn("dbo.Races", "IsArchive", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Races", "IsArchive");
            DropColumn("dbo.Teams", "IsArchive");
            DropColumn("dbo.Games", "IsArchive");
        }
    }
}
