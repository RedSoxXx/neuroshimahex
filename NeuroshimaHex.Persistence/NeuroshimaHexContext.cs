﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity.EntityFramework;
using NeuroshimaHex.Domain.Entities;
using NeuroshimaHex.Domain.Identity;
using NeuroshimaHex.Persistence.Mapping;

namespace NeuroshimaHex.Persistence
{
    public class NeuroshimaHexContext : IdentityDbContext<User, Role, Guid, AspnetUserLogin, AspnetUserRole, AspnetUserClaim>
    {
        public NeuroshimaHexContext()
            : base("NeuroshimaHexContext")
        {
        }

        public DbSet<Game> Games { get; set; }
        public DbSet<Race> Races { get; set; }
        public DbSet<Team> Teams { get; set; }


        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Configurations.Add(new RaceMap());
            modelBuilder.Configurations.Add(new GameMap());
            modelBuilder.Configurations.Add(new TeamMap());
            modelBuilder.Configurations.Add(new UserMap());
            modelBuilder.Configurations.Add(new RoleMap());
        }

        public static NeuroshimaHexContext Create()
        {
            return new NeuroshimaHexContext();
        }
    }
}
