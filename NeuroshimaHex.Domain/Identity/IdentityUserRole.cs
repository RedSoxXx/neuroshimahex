﻿using System;
using Microsoft.AspNet.Identity.EntityFramework;
using NeuroshimaHex.Domain.Entities;

namespace NeuroshimaHex.Domain.Identity
{
    public class AspnetUserRole : IdentityUserRole<Guid>
    {
        public virtual Role Role { get; set; }
    }
}
