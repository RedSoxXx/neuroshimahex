﻿using System;

namespace NeuroshimaHex.Domain.Contracts
{
    public interface IGuidId
    {
       Guid Id { get; set; }
    }
}
