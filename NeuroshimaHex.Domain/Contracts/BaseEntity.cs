﻿using System;

namespace NeuroshimaHex.Domain.Contracts
{
    public class BaseEntity : IGuidId, IArchive
    {
        public Guid Id { get; set; }
        public bool IsArchive { get; set; }
    }
}
