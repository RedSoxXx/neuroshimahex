﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NeuroshimaHex.Domain.Contracts
{
    public interface IArchive
    {
        bool IsArchive { get; set; }
    }
}
