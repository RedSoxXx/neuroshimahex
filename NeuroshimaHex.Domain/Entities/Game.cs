﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NeuroshimaHex.Domain.Contracts;

namespace NeuroshimaHex.Domain.Entities
{
    public class Game : BaseEntity
    {
        public string Name { get; set; }
        public DateTime GameTime { get; set; }
        public bool IsFinished { get; set; }

        public Guid CreatorId { get; set; }

        public virtual User User { get; set; }

        public virtual ICollection<Team> Teams { get; set; }
        public virtual ICollection<UsersInGame> UsersInGames { get; set; }

    }
}
