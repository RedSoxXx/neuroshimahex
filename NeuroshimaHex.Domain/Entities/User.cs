﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using NeuroshimaHex.Domain.Contracts;
using NeuroshimaHex.Domain.Identity;

namespace NeuroshimaHex.Domain.Entities
{
    public class User : IdentityUser<Guid, AspnetUserLogin, AspnetUserRole, AspnetUserClaim>, IGuidId, ICreatedAt, ILastModifiedAt, IArchive
    {
        public User()
        {
            Teams = new List<Team>();
        }
        public string FirstName { get; set; }
        public string LastName { get; set; }

        public DateTime CreatedAt { get; set; }
        public DateTime LastModifiedAt { get; set; }
        public bool IsArchive { get; set; }

        public Guid? FavoriteRaceId { get; set; }


        public virtual ICollection<Team> Teams { get; set; }
        public virtual ICollection<Game> Games { get; set; }
        public virtual Race Race { get; set; }
        public virtual ICollection<UsersInGame> UsersInGames { get; set; }

        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<User, Guid> manager)
        {
            var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
            return userIdentity;
        }

        
    }
}
