﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NeuroshimaHex.Domain.Contracts;

namespace NeuroshimaHex.Domain.Entities
{
    public class Team : BaseEntity
    {
        public Team()
        {
            Users = new List<User>();
        }
        public string TeamName { get; set; }
        public int TeammateCount { get; set; }
        public bool IsWin { get; set; }

        public Guid? GameId { get; set; }

        public virtual ICollection<User> Users { get; set; }
        public virtual Game Game { get; set; }
    }
}
