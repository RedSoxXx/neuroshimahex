﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NeuroshimaHex.Domain.Contracts;

namespace NeuroshimaHex.Domain.Entities
{
    public class Race : BaseEntity
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public byte[] Image { get; set; }

        public virtual ICollection<UsersInGame> UsersInGames { get; set; }
        public virtual ICollection<User> Users { get; set; }
    }
}
