﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NeuroshimaHex.Domain.Contracts;

namespace NeuroshimaHex.Domain.Entities
{
    public class UsersInGame
    {
        [Key, Column(Order = 1)]
        public Guid GameId { get; set; }

        [Key, Column(Order = 2)]
        public Guid UserId { get; set; }

        public Guid RaceId { get; set; }


        public virtual Game Game { get; set; }
        public virtual User User { get; set; }
        public virtual Race Race { get; set; }
    }
}
