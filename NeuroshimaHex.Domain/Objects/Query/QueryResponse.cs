﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NeuroshimaHex.Domain.Objects.Query
{
    public class QueryResponse<T>
    {
        /// <summary>
        /// Gets the data collection.
        /// </summary>
        public List<T> Data { get; set; }
        /// <summary>
        /// Gets the total number of records (without filtering - total dataset).
        /// </summary>
        public int RecordsTotal { get; set; }
        /// <summary>
        /// Gets the resulting number of records after filtering.
        /// </summary>
        public int RecordsFiltered { get; set; }
    }
}
