﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NeuroshimaHex.Domain.Objects.Query
{
    /// <summary>
    /// Defines order directions for proper use.
    /// </summary>
    public enum SortDirection
    {
        /// <summary>
        /// Represents an ascendant (A-Z) ordering.
        /// </summary>
        Asc,
        /// <summary>
        /// Represents a descendant (Z-A) ordering.
        /// </summary>
        Desc
    }
}
