﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NeuroshimaHex.Domain.Objects.Query
{
    public class Paging
    {
        public int Skip { get; set; }
        public int? Length { get; set; }
    }
}
