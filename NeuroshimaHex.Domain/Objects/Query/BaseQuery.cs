﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NeuroshimaHex.Domain.Objects.Query
{
    public class BaseQuery
    {
        public ColumnOrder Orderby { get; set; }
        public Paging Paging { get; set; }
    }
}
