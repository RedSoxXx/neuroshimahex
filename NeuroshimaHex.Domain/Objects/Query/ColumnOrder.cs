﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NeuroshimaHex.Domain.Objects.Query
{
    public class ColumnOrder
    {
        public string Column { get; set; }

        public SortDirection Direction { get; set; }

        public string ToString(string prefix)
        {
            return String.Format("{0}{1} {2}", prefix, Column, Direction.ToString().ToUpperInvariant());
        }
    }
}
