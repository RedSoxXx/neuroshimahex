﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using NeuroshimaHex.Bll.Contracts.Services;

namespace NeuroshimaHex.Bll.Services.Services
{
    public class EmailService : IEmailService
    {
        private readonly string SmtpServerName = "";
        private readonly int Port = 587;
        private readonly bool UseSSL = false;
        private readonly string Username = "";
        private readonly string Password = "";
        private readonly string DefaultFromDisplayName = "The Change Compass Admin";

        public EmailService()
        {
            Username = ConfigurationManager.AppSettings["EmailFromAddress"];
            Password = ConfigurationManager.AppSettings["EmailFromPassword"];
            SmtpServerName = ConfigurationManager.AppSettings["SmtpServerName"];
            UseSSL = ConfigurationManager.AppSettings["EmailUseSSL"].ToLower() == "true";
            var port = 587;
            if (Int32.TryParse(ConfigurationManager.AppSettings["SmtpServerPort"], out port))
            {
                Port = port;
            }
        }

        public void SendEmail(string emailTo, string subject, string text)
        {
            SendEmail(emailTo, subject, text, this.Username, this.DefaultFromDisplayName, true);
        }

        public void SendEmail(IEnumerable<string> recipients, string subject, string text)
        {
            SendEmail(recipients, subject, text, this.Username, this.DefaultFromDisplayName, true);
        }

        public void SendEmail(string emailTo, string subject, string body, string from, string displayFrom, bool isHtml = true)
        {
            SendEmail(SmtpServerName, Port, UseSSL, Username, Password, from, displayFrom, emailTo, subject, body, isHtml);
        }

        public void SendEmail(IEnumerable<string> recipients, string subject, string body, string from, string displayFrom, bool isHtml = true)
        {
            SendEmail(SmtpServerName, Port, UseSSL, Username, Password, from, displayFrom, recipients, subject, body, isHtml);
        }

        private void SendEmail(string smtpServerName, int port, bool useSSL, string username, string password,
            string emailFrom, string displayFrom, string emailTo, string subject, string body, bool isHtml)
        {
            SendEmail(smtpServerName, port, useSSL, username, password, emailFrom, displayFrom, new List<string> { emailTo }, subject, body, isHtml);
        }

        private void SendEmail(string smtpServerName, int port, bool useSSL, string username, string password,
            string emailFrom, string displayFrom, IEnumerable<string> recipients, string subject, string body, bool isHtml)
        {
            using (var message = new MailMessage())
            {
                message.From = new MailAddress(emailFrom, displayFrom);
                foreach (var emailTo in recipients)
                {
                    message.To.Add(new MailAddress(emailTo));
                }
                message.IsBodyHtml = isHtml;
                message.Body = body;
                message.Subject = subject;
                var smtpClient = new SmtpClient(smtpServerName, port)
                {
                    DeliveryMethod = SmtpDeliveryMethod.Network,
                    UseDefaultCredentials = false,
                    EnableSsl = useSSL
                };
                smtpClient.Credentials = new NetworkCredential(username, password);

                if (message.To.Any())
                {
                    smtpClient.Send(message);
                }
            }
        }
    }
}
