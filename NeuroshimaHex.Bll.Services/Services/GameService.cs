﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic;
using System.Text;
using System.Threading.Tasks;
using NeuroshimaHex.Bll.Contracts;
using NeuroshimaHex.Bll.Contracts.Services;
using NeuroshimaHex.Bll.Services.Extensions;
using NeuroshimaHex.Bll.Services.Identity;
using NeuroshimaHex.Domain.Entities;
using NeuroshimaHex.Domain.Objects.Query;

namespace NeuroshimaHex.Bll.Services.Services
{
    public class GameService : BaseService, IGameService
    {
        public GameService(IUnitOfWork uow) : base(uow)
        {
        }

        public Game GetGameById(Guid id)
        {
            return _uow.GetRepository<Game>().FindById(id);
        }

        public void CreateGame(Game game)
        {
            _uow.GetRepository<Game>().Add(game);
            _uow.Commit();
        }

        public void UpdateGame(Game game)
        {
            _uow.GetRepository<Game>().Update(game);
            _uow.Commit();
        }

        public void RemoveGame(Game game)
        {
            _uow.GetRepository<Game>().Remove(game);
            _uow.Commit();
        }

        public void ToArchive(Game game)
        {
            game.IsArchive = true;
            UpdateGame(game);
        }

        public QueryResponse<Game> GetGames(SearchQuery searchQuery)
        {
            var query = _uow.GetRepository<Game>().Where(g=>!g.IsArchive);

            var total = query.Count();

            var search = searchQuery.Search;
            if (!String.IsNullOrEmpty(search))
            {
                query = query.Where(r => r.Name.Contains(search));
            }

            var dateFrom = searchQuery.DateFrom;
            if (dateFrom.HasValue)
            {
                query = query.Where(df => df.GameTime >= dateFrom);
            }

            var dateTo = searchQuery.DateTo;
            if (dateTo.HasValue)
            {
                query = query.Where(df => df.GameTime <= dateTo);
            }

            var totalFiltered = query.Count();

            query = query.OrderByDescending(x => x.GameTime);

            query = query.ApplyPaging(searchQuery.Paging);

            return new QueryResponse<Game>
            {
                Data = query.ToList(),
                RecordsFiltered = totalFiltered,
                RecordsTotal = total
            };
        }

        public int GetGameCount()
        {
            return _uow.GetRepository<Game>().All().Count();
        }
    }
}
