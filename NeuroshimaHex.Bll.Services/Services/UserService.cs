﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic;
using System.Text;
using System.Threading.Tasks;
using NeuroshimaHex.Bll.Contracts;
using NeuroshimaHex.Bll.Contracts.Services;
using NeuroshimaHex.Bll.Services.Extensions;
using NeuroshimaHex.Domain.Entities;
using NeuroshimaHex.Domain.Enums;
using NeuroshimaHex.Domain.Identity;
using NeuroshimaHex.Domain.Objects.Query;

namespace NeuroshimaHex.Bll.Services.Services
{
    public class UserService : BaseService, IUserService
    {
        public UserService(IUnitOfWork uow) : base(uow)
        {
        }

        public User GetById(Guid id)
        {
            return _uow.GetRepository<User>().FindById(id);
        }

        public void Update(User user)
        {
            _uow.GetRepository<User>().Update(user);
            _uow.Commit();
        }

        public void ToArchive(Guid id)
        {
            var user = _uow.GetRepository<User>().FindById(id);
            user.IsArchive = true;
            Update(user);
        }

        public void Delete(User user)
        {
            _uow.GetRepository<User>().Remove(user);
            _uow.Commit();
        }

        public void Delete(Guid id)
        {
            var repo = _uow.GetRepository<User>();
            var user = repo.FindById(id);
            if (user == null)
                return; ;

            repo.Remove(user);
            _uow.Commit();
        }

        public QueryResponse<User> GetUsers(SearchQuery searchQuery)
        {
            var query = _uow.GetRepository<User>().Where(u=>!u.IsArchive);

            var total = query.Count();

            var search = searchQuery.Search;
            if (!String.IsNullOrEmpty(search))
            {
                query = query.Where(r => r.UserName.Contains(search) || r.FirstName.Contains(search) || r.LastName.Contains(search));
            }

            var totalFiltered = query.Count();

            query = query.OrderBy(searchQuery.Orderby.ToString(""));

            query = query.ApplyPaging(searchQuery.Paging);

            return new QueryResponse<User>
            {
                Data = query.ToList(),
                RecordsFiltered = totalFiltered,
                RecordsTotal = total
            };
        }

        public List<User> GetUsers()
        {
            var query = _uow.GetRepository<User>().Where(u => !u.IsArchive);
            return query.ToList();
        }

        public Role GetRole(string name)
        {
            return _uow.GetRepository<Role>().All().First(x => x.Name == name);
        }

        public bool IsAdmin(Guid userId)
        {
            var adminRole = GetRole(UserRoles.Admin);
            return _uow.GetRepository<AspnetUserRole>().All().Any(x => x.UserId == userId && x.RoleId == adminRole.Id);
        }

        public void AddRole(User user, string roleName)
        {
            var role = GetRole(roleName);
            var hasRole = _uow.GetRepository<AspnetUserRole>().All().Any(x => x.UserId == user.Id && x.RoleId == role.Id);
            if (!hasRole)
            {
                user.Roles.Add(new AspnetUserRole
                {
                    UserId = user.Id,
                    RoleId = role.Id
                });
            }
            this.Update(user);
        }

        public void RemoveRole(User user, string roleName)
        {
            var role = GetRole(roleName);
            var userRole = _uow.GetRepository<AspnetUserRole>().All().FirstOrDefault(x => x.UserId == user.Id && x.RoleId == role.Id);
            if (userRole != null)
            {
                _uow.GetRepository<AspnetUserRole>().Remove(userRole);
                _uow.Commit();
            }
        }
    }
}
