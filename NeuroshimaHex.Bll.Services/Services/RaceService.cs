﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic;
using System.Text;
using System.Threading.Tasks;
using NeuroshimaHex.Bll.Contracts;
using NeuroshimaHex.Bll.Contracts.Services;
using NeuroshimaHex.Bll.Services.Extensions;
using NeuroshimaHex.Bll.Services.Identity;
using NeuroshimaHex.Domain.Entities;
using NeuroshimaHex.Domain.Objects.Query;

namespace NeuroshimaHex.Bll.Services.Services
{
    public class RaceService : BaseService, IRaceService
    {

        public RaceService(IUnitOfWork uow)
            : base(uow)
        {
        }

        public Race GetRaceById(Guid id)
        {
            return _uow.GetRepository<Race>().FindById(id);
        }

        public QueryResponse<Race> GetRaces(SearchQuery searchQuery)
        {
            var query = _uow.GetRepository<Race>().Where(r => !r.IsArchive);

            var total = query.Count();

            var search = searchQuery.Search;
            if (!String.IsNullOrEmpty(search))
            {
                query = query.Where(r => r.Name.Contains(search));
            }

            var totalFiltered = query.Count();

            query = query.OrderBy(searchQuery.Orderby.ToString(""));

            query = query.ApplyPaging(searchQuery.Paging);

            return new QueryResponse<Race>
            {
                Data = query.ToList(),
                RecordsFiltered = totalFiltered,
                RecordsTotal = total
            };
        }

        public List<Race> GetRaces()
        {
            return _uow.GetRepository<Race>().Where(r => !r.IsArchive).ToList();
        }

        public void AddRace(Race race)
        {
            _uow.GetRepository<Race>().Add(race);
            _uow.Commit();
        }

        public void UpdateRace(Race race)
        {
            _uow.GetRepository<Race>().Update(race);
            _uow.Commit();
        }

        public void RemoveRace(Race race)
        {
            _uow.GetRepository<Race>().Remove(race);
            _uow.Commit();
        }

        public void ToArchive(Race race)
        {
            race.IsArchive = true;
            UpdateRace(race);
        }

        public List<Guid> GetWonRacesId(SearchQuery searchQuery)
        {
            var finishedGames = _uow.GetRepository<Game>().Where(g => g.IsFinished);

            if (searchQuery.DateFrom.HasValue)
            {
                finishedGames = finishedGames.Where(g => g.GameTime >= searchQuery.DateFrom.Value);
            }

            if (searchQuery.DateTo.HasValue)
            {
                finishedGames = finishedGames.Where(g => g.GameTime <= searchQuery.DateTo.Value);
            }

            var wonsRacesInGames =
                finishedGames
                    .Select(
                        g =>
                            g.Teams.Where(t => t.IsWin)
                                .Select(
                                    t =>
                                        t.Users.Select(
                                            u =>
                                                u.UsersInGames.Where(
                                                        us => us.GameId == t.GameId && us.UserId == u.Id)
                                                    .Select(usi => usi.Race.Id))))
                    .ToList();
            return (from raceIdinGame in wonsRacesInGames from raceId in raceIdinGame from id in raceId from guid in id select guid).ToList();
        }
    }
}
