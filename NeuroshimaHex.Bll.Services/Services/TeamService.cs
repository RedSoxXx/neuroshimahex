﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic;
using System.Text;
using System.Threading.Tasks;
using NeuroshimaHex.Bll.Contracts;
using NeuroshimaHex.Bll.Contracts.Services;
using NeuroshimaHex.Bll.Services.Extensions;
using NeuroshimaHex.Bll.Services.Identity;
using NeuroshimaHex.Domain.Entities;
using NeuroshimaHex.Domain.Objects.Query;

namespace NeuroshimaHex.Bll.Services.Services
{
    public class TeamService : BaseService, ITeamService
    {
        public TeamService(IUnitOfWork uow) : base(uow)
        {
        }

        public Team GetTeamById(Guid id)
        {
            return _uow.GetRepository<Team>().FindById(id);
        }

        public void CreateTeam(Team team)
        {
            _uow.GetRepository<Team>().Add(team);
            _uow.Commit();
        }

        public void UpdateTeam(Team team)
        {
            _uow.GetRepository<Team>().Update(team);
            _uow.Commit();
        }

        public void UpdateTeam(List<Team> teams)
        {
            try
            {
                _uow.AutoDetectChanges = false;

                var teamsRepo = _uow.GetRepository<Team>();
                teams.ForEach(i => teamsRepo.Update(i));
                _uow.Commit();
            }
            finally
            {
                _uow.AutoDetectChanges = true;
            }
        }

       

        public void RemoveTeam(Team team)
        {
            _uow.GetRepository<Team>().Remove(team);
            _uow.Commit();
        }

        public QueryResponse<Team> GetTeams(SearchQuery searchQuery)
        {
            var query = _uow.GetRepository<Team>().Where(t => !t.IsArchive);

            var total = query.Count();

            var search = searchQuery.Search;
            if (!String.IsNullOrEmpty(search))
            {
                query = query.Where(r => r.TeamName.Contains(search));
            }

            var totalFiltered = query.Count();

            query = query.OrderBy(searchQuery.Orderby.ToString("")); 

            query = query.ApplyPaging(searchQuery.Paging);

            return new QueryResponse<Team>
            {
                Data = query.ToList(),
                RecordsFiltered = totalFiltered,
                RecordsTotal = total
            };
        }
    }
}
