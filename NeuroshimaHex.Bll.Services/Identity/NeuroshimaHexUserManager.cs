﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;
using Microsoft.Owin.Security.DataProtection;
using NeuroshimaHex.Domain.Entities;
using NeuroshimaHex.Persistence;

namespace NeuroshimaHex.Bll.Services.Identity
{
    public class NeuroshimaHexUserManager : UserManager<User, Guid>
    {
        public NeuroshimaHexUserManager(NeuroshimaHexUserStore store) 
            : base(store)
        {
        }

        public static NeuroshimaHexUserManager Create(IdentityFactoryOptions<NeuroshimaHexUserManager> options,
            IOwinContext context)
        {
            var manager = new NeuroshimaHexUserManager(new NeuroshimaHexUserStore(context.Get<NeuroshimaHexContext>()));
            var provider = new DpapiDataProtectionProvider("Sample");
            manager.UserTokenProvider = new DataProtectorTokenProvider<User, Guid>(provider.Create("EmailConfirmation"));
            return manager;
        }
    }
}
