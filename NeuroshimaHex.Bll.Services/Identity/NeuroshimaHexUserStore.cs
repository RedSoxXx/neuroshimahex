﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity.EntityFramework;
using NeuroshimaHex.Domain.Entities;
using NeuroshimaHex.Domain.Identity;

namespace NeuroshimaHex.Bll.Services.Identity
{
    public class NeuroshimaHexUserStore : UserStore<User, Role, Guid, AspnetUserLogin, AspnetUserRole, AspnetUserClaim>
    {
        public NeuroshimaHexUserStore(DbContext context) 
            : base(context)
        {
        }
    }
}
