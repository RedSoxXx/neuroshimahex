﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NeuroshimaHex.Domain.Objects.Query;

namespace NeuroshimaHex.Bll.Services.Extensions
{
    public static class QueryExtensions
    {
        public static IQueryable<T> ApplyPaging<T>(this IQueryable<T> queryable, Paging paging)
        {
            if (paging.Skip > 0)
            {
                queryable = queryable.Skip(paging.Skip);
            }

            if (paging.Length.HasValue && paging.Length.Value >= 0)
            {
                queryable = queryable.Take(paging.Length.Value);
            }
            return queryable;
        }

        public static IEnumerable<T> ApplyPaging<T>(this IEnumerable<T> list, Paging paging)
        {
            if (paging.Skip > 0)
            {
                list = list.Skip(paging.Skip);
            }

            if (paging.Length.HasValue && paging.Length.Value >= 0)
            {
                list = list.Take(paging.Length.Value);
            }
            return list;
        }
    }
}
